const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
const port = process.env.PORT || 5000

// Middleware
app.use(bodyParser.json())
app.use(cors())

const suppliers = require('./routes/api/suppliers');
app.use('/api/suppliers', suppliers);

const deliveries = require('./routes/api/deliveries');
app.use('/api/deliveries', deliveries);

const chambers = require('./routes/api/chambers');
app.use('/api/chambers', chambers);

const raports = require('./routes/api/raports');
app.use('/api/raports', raports);


// Handle production
if (process.env.NODE_ENV === 'production') { // true on heroku
    app.use(express.static(__dirname + '/public/'))

    // Handle SPA
    app.get(/.*/, (req, res) => {
        res.sendFile(__dirname + '/public/index.html')
    })
}

app.listen(port, () => {
    console.log(`Serwer started on port ${port}`)
})

