const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()

// Get raports
router.get('/', async (req, res) => {
    const dataFromDB = await loadData()
    // res.send(await dataFromDB.find({}).toArray())    // wszystkie raporty za długo się wczytują, trzeba kiedyś zrobić inne ładowanie...
    res.send(await dataFromDB.find({}).sort({_id:-1}).limit(60).toArray()) //... na razie ograniczenie liczby do 15 najnwszych sztuk
})


// Add raport
router.post('/', async (req, res) => {
    const dataFromDB = await loadData()
    await dataFromDB.insertOne(req.body)
    res.status(201).send()
})


// Delete raport
router.delete('/:dateTime', async (req, res) => {
    const dataFromDB = await loadData()

    try {

    } catch (error) {
        throw error;
    }

    //console.log('DELETE: ' + req.params.dateTime)

    // delete one element
    // await dataFromDB.deleteOne({
    //     _id: new mongodb.ObjectID(req.params.id)
    // })

    await dataFromDB.deleteMany({
        "dateTime": {
            $gt: req.params.dateTime
        }
    });

    res.status(202).send()
})



async function loadData() {
    const client = await mongodb.MongoClient.connect(
        // 'mongodb://pawel:wihajster123@ds057548.mlab.com:57548/mlyngliwice', {
        'mongodb://localhost:27017/mlyngliwice', {
        useNewUrlParser: true
    }
    )
    return client.db('mlyngliwice').collection('fullRaports')
}

module.exports = router