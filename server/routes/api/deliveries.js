const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()


// Get deliveries
router.get('/', async (req, res) => {
  const dataFromDB = await loadData()

  if (req.query.getAll) { // pobierz wszystkie dane
    res.send(await dataFromDB.find({}).toArray())
  } else {
    res.send(await dataFromDB.find({
      // wyślij jeśli choć jeden silos jest większy od zera
      "$or": [{
        "act_s1": {
          "$gt": 0
        }
      },
      {
        "act_s2": {
          "$gt": 0
        }
      },
      {
        "act_s3": {
          "$gt": 0
        }
      },
      {
        "act_s4": {
          "$gt": 0
        }
      },
      ]
    }).toArray())
  }

})

// Add delivery
router.post('/', async (req, res) => {
  const dataFromDB = await loadData()

  await dataFromDB.insertOne({
    name: req.body.name,
    date: req.body.date,
    time: req.body.time,
    in_s1: parseFloat(req.body.in_s1),
    in_s2: parseFloat(req.body.in_s2),
    in_s3: parseFloat(req.body.in_s3),
    in_s4: parseFloat(req.body.in_s4),
    act_s1: parseFloat(req.body.act_s1),
    act_s2: parseFloat(req.body.act_s2),
    act_s3: parseFloat(req.body.act_s3),
    act_s4: parseFloat(req.body.act_s4)
  })
  res.status(201).send()
})

// Update delivery
router.put('/', async (req, res) => {
  const dataFromDB = await loadData()

  //console.log('UPDATE DELIVERY')
  //console.log(req.body)

  if (Array.isArray(req.body)) { // paczka dokumentów do zaktualizowania
    //console.log('UPDATE DELIVERY - array')
    var ops = []
    req.body.forEach(item => {
      ops.push({
        updateOne: {
          filter: {
            _id: new mongodb.ObjectID(item._id)
          },
          update: {
            $set: {
              name: item.name,
              time: item.time,
              date: item.date,
              in_s1: parseFloat(item.in_s1),
              in_s2: parseFloat(item.in_s2),
              in_s3: parseFloat(item.in_s3),
              in_s4: parseFloat(item.in_s4),

              act_s1: parseFloat(item.act_s1),
              act_s2: parseFloat(item.act_s2),
              act_s3: parseFloat(item.act_s3),
              act_s4: parseFloat(item.act_s4)
            }
          },
          upsert: true
        }
      })
    })

    await dataFromDB.bulkWrite(ops, {
      ordered: false
    });
    res.status(202).send()
  } else { // tylko jeden dokument do zaktualizowania
    var myquery = {
      _id: new mongodb.ObjectID(req.body._id)
    };
    var newvalues = {
      $set: {
        name: req.body.name,
        date: req.body.date,
        time: req.body.time,
        in_s1: parseFloat(req.body.in_s1),
        in_s2: parseFloat(req.body.in_s2),
        in_s3: parseFloat(req.body.in_s3),
        in_s4: parseFloat(req.body.in_s4),
        act_s1: parseFloat(req.body.act_s1),
        act_s2: parseFloat(req.body.act_s2),
        act_s3: parseFloat(req.body.act_s3),
        act_s4: parseFloat(req.body.act_s4)
      }
    };
    await dataFromDB.updateOne(myquery, newvalues)
    res.status(202).send()
  }

})

// Delete delivery
router.delete('/:id', async (req, res) => {
  const dataFromDB = await loadData()

  //console.log('DELETE DELIV')
  //console.log(req.body.date.substring(0, 10))

  if (req.body.date !== undefined) { // skasuj wszystkie elemnty starsze od podanej daty
    //console.log('kasuje DATE')
    await dataFromDB.deleteMany({
      "date": {
        $gt: req.body.date.substring(0, 10)
      }
    });
  } else { //skasuj elemet z podanym id
    //console.log('kasuje ID')
    await dataFromDB.deleteOne({
      _id: new mongodb.ObjectID(req.params.id)
    })
  }




  res.status(202).send()
})



async function loadData() {
  const client = await mongodb.MongoClient.connect(
    // 'mongodb://pawel:wihajster123@ds057548.mlab.com:57548/mlyngliwice', {
    'mongodb://localhost:27017/mlyngliwice', {
    useNewUrlParser: true
  }
  )
  return client.db('mlyngliwice').collection('deliveries')
}

module.exports = router