const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()

// Get values from chambers
router.get('/', async (req, res) => {
  const dataFromDB = await loadData()

  if (req.query.date !== undefined) {
    res.send(await dataFromDB.find({
      "date": {
        "$eq": req.query.date
      }
    }).toArray())
  } else {
    res.send(await dataFromDB.find({
      // wyślij jeśli choć jeden silos jest większy od zera
      "$or": [{
        "act_ch1": {
          "$gt": 0
        }
      },
      {
        "act_ch2": {
          "$gt": 0
        }
      },
      {
        "act_ch3": {
          "$gt": 0
        }
      },
      {
        "act_ch4": {
          "$gt": 0
        }
      },
      ]
    }).toArray())
  }

})


// Add chamber
router.post('/', async (req, res) => {
  const dataFromDB = await loadData()

  if (Array.isArray(req.body)) {
    //console.log('insert many')
    //console.log(req.body)
    req.body.forEach(item => {
      item._id = new mongodb.ObjectID(item._id)
    })

    await dataFromDB.insertMany(req.body);
  } else {
    //console.log('insert one')
    await dataFromDB.insertOne({
      date: req.body.date,
      in_ch1: parseFloat(req.body.in_ch1),
      in_ch2: parseFloat(req.body.in_ch2),
      in_ch3: parseFloat(req.body.in_ch3),
      in_ch4: parseFloat(req.body.in_ch4),
      act_ch1: parseFloat(req.body.act_ch1),
      act_ch2: parseFloat(req.body.act_ch2),
      act_ch3: parseFloat(req.body.act_ch3),
      act_ch4: parseFloat(req.body.act_ch4),
      suppliers: req.body.suppliers
    })
  }

  res.status(201).send()
})



// Update chamber
router.put('/', async (req, res) => {
  const dataFromDB = await loadData()

  //console.log('CHAMBERS UPDATE')
  //console.log(req.body)

  var ops = []
  req.body.forEach(item => {
    ops.push({
      updateOne: {
        filter: {
          _id: new mongodb.ObjectID(item._id)
        },
        update: {
          $set: {
            date: item.date,
            in_ch1: parseFloat(item.in_ch1),
            in_ch2: parseFloat(item.in_ch2),
            in_ch3: parseFloat(item.in_ch3),
            in_ch4: parseFloat(item.in_ch4),
            act_ch1: parseFloat(item.act_ch1),
            act_ch2: parseFloat(item.act_ch2),
            act_ch3: parseFloat(item.act_ch3),
            act_ch4: parseFloat(item.act_ch4),
            suppliers: item.suppliers
          },
        },
        upsert: false
      }
    })
  })

  //console.log(ops[0].updateOne.update)

  let result = await dataFromDB.bulkWrite(ops, {
    ordered: false
  });

  //console.log(result)

  res.status(202).send()
})


// Delete chamber
router.delete('/:dateTime', async (req, res) => {
  const dataFromDB = await loadData()

  await dataFromDB.deleteMany({}); // delete all elements

  // await dataFromDB.deleteOne({ // delete one element
  //     _id: new mongodb.ObjectID(req.params.id)
  // })

  // await dataFromDB.deleteMany({
  //     "dateTime": {
  //         $gt: req.params.dateTime
  //     }
  // });

  res.status(202).send()
})


async function loadData() {
  const client = await mongodb.MongoClient.connect(
    // 'mongodb://pawel:wihajster123@ds057548.mlab.com:57548/mlyngliwice', {
    'mongodb://localhost:27017/mlyngliwice', {
    useNewUrlParser: true
  }
  )
  return client.db('mlyngliwice').collection('chambers')
}

module.exports = router