const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()

// Get suppliers
router.get('/', async (req, res) => {
  //console.log('GET SUPPLIERS')
  const suppliers = await loadData()
  res.send(await suppliers.find({}).toArray())

})

// Add supplier
router.post('/', async (req, res) => {
  // console.log(`Add supplier`);
  const suppliers = await loadData()
  await suppliers.insertOne({
    name: req.body.name,
    zip: req.body.zip,
    street: req.body.street,
    city: req.body.city,
    nr: req.body.nr,
    nip: req.body.nip,
    tel: req.body.tel
  })
  res.status(201).send()
})

// Update supplier
router.put('/', async (req, res) => {
  const suppliers = await loadData()

  var myquery = { _id: new mongodb.ObjectID(req.body._id) };
  var newvalues = {
    $set: {
      name: req.body.name,
      zip: req.body.zip,
      street: req.body.street,
      city: req.body.city,
      nr: req.body.nr,
      nip: req.body.nip,
      tel: req.body.tel
    }
  };

  await suppliers.updateOne(myquery, newvalues)
  res.status(202).send()
})

// Delete supplier
router.delete('/:id', async (req, res) => {
  const suppliers = await loadData()
  await suppliers.deleteOne({ _id: new mongodb.ObjectID(req.params.id) })
  res.status(202).send()
})


async function loadData() {
  const client = await mongodb.MongoClient.connect(
    // 'mongodb://pawel:wihajster123@ds057548.mlab.com:57548/mlyngliwice', {
    'mongodb://localhost:27017/mlyngliwice', {
    useNewUrlParser: true
  }
  )
  return client.db('mlyngliwice').collection('suppliers')
}

module.exports = router