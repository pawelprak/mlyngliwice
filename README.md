# Dev  
### Serwer    
Uruchomienie serwera:  
`npm install`  - if it is a first time run  
`npm run dev`  

### Client  
Uruchomienie klienta:  
`cd client`  
`npm install` - if it is a first time run  
`npm run serve`  

# Build
Eksport na heroku, w folderze serwera wpisywać po kolei:  
`cd client`  
`npm run build`  
`cd ..`      
`git add .`  
`git commit -m "komentarz"`  
`git push heroku master`  s
`heroku open`  

# ToDo  
- suppliers - sprawdzenie czy nie ma dwóch takich samych dostawców    
- serwer - ujednolicić, zasada DRY: Don't Repeat Yourself  
- db - czyszczenie wszystkiego oprócz raportów (puste dokumenty i starsze niż miesiąc)  
- db - backup bazy na dysk  
- raporty - export do markdown a potem do pdf  
- Stworzenie systemu autoryzacji użytkowników  
- komory - edycja bazy danych z komorami leżakowymi  
- db - wyłapać błedy mongodb try/catch  
- raporty - wyświetlanie (pobieranie z db) max po 15 raportów  
- ~~fonty i czcionki nie przez CDN~~    
- ~~silos - wymusić aby użytkownik wpisał nazwę dostawcy (pole required)~~  
- ~~silos - edycja powinna chować pola z dnia dostawy a nowa dostawa powinna chować pola z wartością aktualną~~   
- ~~komory - wywalić "Czy pobrać z dzisiejszego dnia" - dzisiejszą dostawę dodać do bazy danych jeszcze przed przeliczeniami~~    
- ~~komory - pole "wybierz date" puse - bez uzupełnienia nie wolno wykonać operacji ZAPISZ!~~    
- ~~komory - automatyczne wyliczenie procentów pobrania z silosów~~  
- ~~db - po zapisie do DB wyświtlić raport co zmodyfikowano, co wstawiono itp~~   
- ~~silos - przycisk do wyświtlenia wszystkich dostaw, włącznie już z tymi wyzerowanymi~~    
- ~~suppliers - dodać nr telefonu~~  
- ~~silos - dodać zabezpieczenia na typ wpisywanych danych do silosów (ma być liczba)~~  



