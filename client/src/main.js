import Vue from 'vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


// Vue.use({
//     iconfont: 'md'
//   })
