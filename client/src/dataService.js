import axios from 'axios';

// const url = 'http://localhost:5000/api/suppliers/'

class DataService {
  // get suppliers
  static getData(url, _getAll, _date) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url, {
          params: {
            getAll: _getAll,
            date: _date
          }
        });
        resolve(res.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  // add supplier
  static insertData(url, data) {
    return axios
      .post(url, data)
      .then((response) => {
        // console.log(response)
      })
      .catch((error) => {
        console.log(error);
      });
  }

  // update supplier
  static updateData(url, data) {
    return axios.put(url, data);
  }

  // delete supplier
  static deleteData(url, id, _date) {
    //console.log(_date)
    return axios.delete(`${url}${id}`, {
      data: {
        date: _date
      }
    });
  }
}

export default DataService;
