import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Chambers from './views/Chambers.vue'
import Silos from './views/Silos.vue'
import Suppliers from './views/Suppliers.vue'
import History from './views/History.vue'
import Raports from './views/Raports.vue'
import Help from './views/Help.vue'
import MongoService from './views/MongoService.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },


    {
        path: '/silos',
        name: 'silos',
        component: Silos
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/chambers',
      name: 'chambers',
      component: Chambers
    },
    {
      path: '/mongoService',
      name: 'mongoService',
      component: MongoService
    },
    {
      path: '/suppliers',
      name: 'suppliers',
      component: Suppliers
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '/raports',
      name: 'raports',
      component: Raports
    },
    {
      path: '/help',
      name: 'help',
      component: Help
    },




    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
